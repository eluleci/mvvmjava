package com.eluleci.lastfm.service;

import com.eluleci.lastfm.service.response.GetTrackInfoResponse;
import com.eluleci.lastfm.service.response.GetTrackSummaryListResponse;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Interactor {

    @Inject
    public LastFMService mService;

    public Interactor(LastFMService service) {
        mService = service;
    }

    public void searchTracks(String searchInput, final GetTrackListListener callback) {
        mService.searchTracks(searchInput).enqueue(new Callback<GetTrackSummaryListResponse>() {
            @Override
            public void onResponse(Call<GetTrackSummaryListResponse> call, Response<GetTrackSummaryListResponse> response) {
                callback.onReceiveTrackListResponse(response.isSuccessful(), response.body());
            }

            @Override
            public void onFailure(Call<GetTrackSummaryListResponse> call, Throwable t) {
                callback.onReceiveTrackListResponse(false, null);
            }
        });
    }

    public interface GetTrackListListener {
        void onReceiveTrackListResponse(boolean isSuccessful, GetTrackSummaryListResponse response);
    }

    public void getTrackDetail(String artist, String track, final GetTrackInfoListener callback) {
        mService.getTrackDetail(artist, track).enqueue(new Callback<GetTrackInfoResponse>() {
            @Override
            public void onResponse(Call<GetTrackInfoResponse> call, Response<GetTrackInfoResponse> response) {
                callback.onReceiveTrackResponse(response.isSuccessful(), response.body());
            }

            @Override
            public void onFailure(Call<GetTrackInfoResponse> call, Throwable t) {
                callback.onReceiveTrackResponse(false, null);
            }
        });
    }

    public interface GetTrackInfoListener {
        void onReceiveTrackResponse(boolean isSuccessful, GetTrackInfoResponse response);
    }
}
