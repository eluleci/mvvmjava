package com.eluleci.lastfm.ui.tracklist;

import javax.inject.Singleton;

import dagger.Module;

@Module
public abstract class TrackListModule {

    @Singleton
    abstract TrackListViewModelFactory provideTrackListViewModelFactory();
}
