package com.eluleci.lastfm.ui.trackdetail;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.eluleci.lastfm.R;
import com.eluleci.lastfm.databinding.ActivityTrackDetailBinding;
import com.eluleci.lastfm.models.TrackSummary;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class TrackDetailActivity extends DaggerAppCompatActivity {

    public static String PARAM_SONG = "com.eluleci.lastfm.ui.songdetail.TrackDetailActivity.song";

    public static Intent generateIntent(Context context, TrackSummary trackSummary) {
        Intent intent = new Intent(context, TrackDetailActivity.class);
        intent.putExtra(TrackDetailActivity.PARAM_SONG, trackSummary);
        return intent;
    }

    @Inject
    public TrackSummary mTrackSummary;

    @Inject
    public TrackDetailViewModelFactory mTrackDetailViewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("FACTORY");
        System.out.println(mTrackDetailViewModelFactory);

        if (getIntent().getExtras() == null || !getIntent().hasExtra(PARAM_SONG)) {
            throw new RuntimeException("TrackSummary id must be provided with intent extras.");
        }

        ActivityTrackDetailBinding binding = DataBindingUtil
                .setContentView(this, R.layout.activity_track_detail);
        binding.setLifecycleOwner(this);

        TrackDetailViewModel viewModel = ViewModelProviders.of(
                this, mTrackDetailViewModelFactory
        ).get(TrackDetailViewModel.class);

        binding.setViewModel(viewModel);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mTrackSummary.getName());
        }
    }
}
