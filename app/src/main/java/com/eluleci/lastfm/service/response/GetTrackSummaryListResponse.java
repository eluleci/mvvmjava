package com.eluleci.lastfm.service.response;

import com.google.gson.annotations.SerializedName;

public class GetTrackSummaryListResponse {
    @SerializedName("results")
    public SearchResult results;

    public GetTrackSummaryListResponse(SearchResult results) {
        this.results = results;
    }
}

