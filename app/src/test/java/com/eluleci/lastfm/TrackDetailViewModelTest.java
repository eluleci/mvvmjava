package com.eluleci.lastfm;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.eluleci.lastfm.models.Album;
import com.eluleci.lastfm.models.Artist;
import com.eluleci.lastfm.models.Track;
import com.eluleci.lastfm.models.TrackSummary;
import com.eluleci.lastfm.service.Interactor;
import com.eluleci.lastfm.service.response.GetTrackInfoResponse;
import com.eluleci.lastfm.ui.trackdetail.TrackDetailViewModel;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TrackDetailViewModelTest {

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Test
    public void tractDetail_checkError() {

        TrackSummary trackSummary = new TrackSummary();
        trackSummary.setArtist("Nirvana");
        trackSummary.setName("Smells Like Teen Spirit");

        TrackDetailViewModel viewModel = new TrackDetailViewModel(new Interactor(null) {
            @Override
            public void getTrackDetail(String artist, String track, GetTrackInfoListener callback) {
                callback.onReceiveTrackResponse(false, new GetTrackInfoResponse());
            }
        }, trackSummary);

        assertNull(viewModel.getName().getValue());
        assertTrue(viewModel.getShowErrorMessage().getValue());
    }

    @Test
    public void tractDetail_checkSuccess() {

        TrackSummary trackSummary = new TrackSummary();
        trackSummary.setArtist("Nirvana");
        trackSummary.setArtist("Smells Like Teen Spirit");

        final Track returnedTrack = new Track();
        returnedTrack.setArtist(new Artist("Nirvana"));
        returnedTrack.setName("Smells Like Teen Spirit");
        returnedTrack.setDuration("238000");
        returnedTrack.setAlbum(new Album("Nirvana", "Something", null));

        TrackDetailViewModel viewModel = new TrackDetailViewModel(new Interactor(null) {
            @Override
            public void getTrackDetail(String artist, String track, GetTrackInfoListener callback) {
                callback.onReceiveTrackResponse(true, new GetTrackInfoResponse(
                        returnedTrack
                ));
            }
        }, trackSummary);

        assertNotNull(viewModel.getName().getValue());
        assertFalse(viewModel.getShowErrorMessage().getValue());
    }
}
