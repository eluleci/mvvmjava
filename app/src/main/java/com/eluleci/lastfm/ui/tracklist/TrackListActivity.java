package com.eluleci.lastfm.ui.tracklist;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.eluleci.lastfm.R;
import com.eluleci.lastfm.databinding.ActivitySearchBinding;
import com.eluleci.lastfm.models.TrackSummary;
import com.eluleci.lastfm.ui.trackdetail.TrackDetailActivity;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class TrackListActivity extends DaggerAppCompatActivity implements TrackListAdapter.OnClickListener {

    @Inject
    public TrackListViewModelFactory mTrackListViewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivitySearchBinding binding = DataBindingUtil
                .setContentView(this, R.layout.activity_search);
        binding.setLifecycleOwner(this);

        binding.recyclerView.setAdapter(new TrackListAdapter(this));

        TrackListViewModel viewModel = ViewModelProviders
                .of(this, mTrackListViewModelFactory)
                .get(TrackListViewModel.class);
        binding.setViewModel(viewModel);
    }

    @Override
    public void onItemClick(TrackSummary trackSummary) {
        startActivity(TrackDetailActivity.generateIntent(this, trackSummary));
    }
}
