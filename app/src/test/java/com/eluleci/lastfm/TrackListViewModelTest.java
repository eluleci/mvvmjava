package com.eluleci.lastfm;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.eluleci.lastfm.models.TrackSummary;
import com.eluleci.lastfm.service.Interactor;
import com.eluleci.lastfm.service.LastFMService;
import com.eluleci.lastfm.service.response.GetTrackSummaryListResponse;
import com.eluleci.lastfm.service.response.SearchResult;
import com.eluleci.lastfm.service.response.Trackmatches;
import com.eluleci.lastfm.ui.tracklist.TrackListViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TrackListViewModelTest {

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    public LastFMService mMockService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void tractList_checkEmpty() {

        TrackListViewModel viewModel = new TrackListViewModel(new Interactor(null) {
            @Override
            public void searchTracks(String searchInput, GetTrackListListener callback) {
                callback.onReceiveTrackListResponse(
                        true,
                        new GetTrackSummaryListResponse(new SearchResult(
                                new Trackmatches(
                                        new ArrayList<TrackSummary>()
                                )
                        )));
            }
        });

        viewModel.getTrackSummaries("Jack");

        assertEquals(0, viewModel.getTrackSummaries().getValue().size());
        assertTrue(viewModel.getShowEmptyMessage().getValue());
        assertFalse(viewModel.getShowErrorMessage().getValue());
    }

    @Test
    public void tractList_checkError() {

        TrackListViewModel viewModel = new TrackListViewModel(new Interactor(null) {
            @Override
            public void searchTracks(String searchInput, GetTrackListListener callback) {
                callback.onReceiveTrackListResponse(
                        false,
                        new GetTrackSummaryListResponse(new SearchResult(
                                new Trackmatches(
                                        new ArrayList<TrackSummary>()
                                )
                        )));
            }
        });

        viewModel.getTrackSummaries("Jack");

        assertNull(viewModel.getTrackSummaries().getValue());
        assertFalse(viewModel.getShowEmptyMessage().getValue());
        assertTrue(viewModel.getShowErrorMessage().getValue());
    }

    @Test
    public void tractList_checkNotEmpty() {

        final List<TrackSummary> list = new ArrayList<>();
        list.add(new TrackSummary());
        list.add(new TrackSummary());
        list.add(new TrackSummary());
        list.add(new TrackSummary());

        TrackListViewModel viewModel = new TrackListViewModel(new Interactor(null) {
            @Override
            public void searchTracks(String searchInput, GetTrackListListener callback) {
                callback.onReceiveTrackListResponse(
                        true,
                        new GetTrackSummaryListResponse(new SearchResult(
                                new Trackmatches(list)
                        )));
            }
        });

        viewModel.getTrackSummaries("Jack");

        assertEquals(4, viewModel.getTrackSummaries().getValue().size());
        assertFalse(viewModel.getShowEmptyMessage().getValue());
        assertFalse(viewModel.getShowErrorMessage().getValue());
    }
}