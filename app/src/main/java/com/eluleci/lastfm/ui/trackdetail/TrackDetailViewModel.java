package com.eluleci.lastfm.ui.trackdetail;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.eluleci.lastfm.models.Image;
import com.eluleci.lastfm.models.Track;
import com.eluleci.lastfm.models.TrackSummary;
import com.eluleci.lastfm.service.Interactor;
import com.eluleci.lastfm.service.response.GetTrackInfoResponse;

import javax.inject.Inject;

public class TrackDetailViewModel extends ViewModel implements Interactor.GetTrackInfoListener {

    public Interactor mService;

    private MutableLiveData<String> mCoverUrl = new MutableLiveData<>();
    private MutableLiveData<String> mName = new MutableLiveData<>();
    private MutableLiveData<String> mArtist = new MutableLiveData<>();
    private MutableLiveData<String> mDuration = new MutableLiveData<>();
    private MutableLiveData<String> mListeners = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsFetching = new MutableLiveData<>();
    private MutableLiveData<Boolean> mShowErrorMessage = new MutableLiveData<>();

    @Inject
    public TrackDetailViewModel(Interactor interactor, TrackSummary trackSummary) {
        this.mService = interactor;

        mIsFetching.setValue(false);
        mShowErrorMessage.setValue(false);

        fetchTrackInfo(trackSummary);
    }

    public MutableLiveData<Boolean> getIsFetching() {
        return mIsFetching;
    }

    public MutableLiveData<Boolean> getShowErrorMessage() {
        return mShowErrorMessage;
    }

    public MutableLiveData<String> getCoverUrl() {
        return mCoverUrl;
    }

    public MutableLiveData<String> getName() {
        return mName;
    }

    public MutableLiveData<String> getArtist() {
        return mArtist;
    }

    public MutableLiveData<String> getDuration() {
        return mDuration;
    }

    public MutableLiveData<String> getListeners() {
        return mListeners;
    }

    private void fetchTrackInfo(TrackSummary trackSummary) {

        mIsFetching.setValue(true);
        mShowErrorMessage.setValue(false);

        mService.getTrackDetail(trackSummary.getArtist(), trackSummary.getName(), this);
    }

    @Override
    public void onReceiveTrackResponse(boolean success, GetTrackInfoResponse response) {
        Track track = response.track;

        mIsFetching.setValue(false);

        if (!success || track == null) {
            mShowErrorMessage.setValue(true);
            return;
        }

        mName.setValue(track.getName());
        mArtist.setValue(track.getArtist().getName());
        mDuration.setValue(track.getDurationFormatted());
        mListeners.setValue(track.getListeners());
        if (track.getAlbum().getImage(Image.Size.extralarge) != null) {
            mCoverUrl.setValue(track.getAlbum().getImage(Image.Size.extralarge).getUrl());
        }

    }
}