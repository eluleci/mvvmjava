package com.eluleci.lastfm.ui;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.databinding.BindingAdapter;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.eluleci.lastfm.models.TrackSummary;
import com.eluleci.lastfm.ui.tracklist.TrackListAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BindingUtils {

    @BindingAdapter("trackSummaries")
    public static void setTrackSummaries(final RecyclerView view, MutableLiveData<List<TrackSummary>> songs) {
        if (view.getAdapter() instanceof TrackListAdapter) {
            songs.observe((LifecycleOwner) view.getContext(), new Observer<List<TrackSummary>>() {
                @Override
                public void onChanged(@Nullable List<TrackSummary> trackSummaries) {
                    ((TrackListAdapter) view.getAdapter()).setTrackSummaries(trackSummaries);
                }
            });
        }
    }

    @BindingAdapter("visibility")
    public static void setVisibility(final View view, MutableLiveData<Boolean> value) {
        view.setVisibility(value.getValue() ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            Picasso.get().load(imageUrl).into(view);
        }
    }
}
