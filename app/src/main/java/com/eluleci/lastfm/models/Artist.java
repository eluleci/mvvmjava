package com.eluleci.lastfm.models;

import java.io.Serializable;

public class Artist implements Serializable {
    private String name;
    private String mbid;
    private String url;

    public Artist() {
    }

    public Artist(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getMbid() {
        return mbid;
    }

    public String getUrl() {
        return url;
    }
}
