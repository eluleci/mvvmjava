package com.eluleci.lastfm.ui.tracklist;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.text.Editable;
import android.text.TextWatcher;

import com.eluleci.lastfm.models.TrackSummary;
import com.eluleci.lastfm.service.Interactor;
import com.eluleci.lastfm.service.response.GetTrackSummaryListResponse;

import java.util.List;

import javax.inject.Inject;

public class TrackListViewModel extends ViewModel implements Interactor.GetTrackListListener {

    public Interactor mService;

    private MutableLiveData<String> mSearchInput = new MutableLiveData<>();
    private TextWatcher mWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!mSearchInput.getValue().equals(s.toString())) {
                mSearchInput.setValue(s.toString());
                fetchSongs(s.toString());
            }
        }
    };

    private MutableLiveData<List<TrackSummary>> mSongs = new MutableLiveData<>();
    private MutableLiveData<Boolean> mIsFetching = new MutableLiveData<>();
    private MutableLiveData<Boolean> mShowEmptyMessage = new MutableLiveData<>();
    private MutableLiveData<Boolean> mShowErrorMessage = new MutableLiveData<>();

    @Inject
    public TrackListViewModel(Interactor interactor) {
        this.mService = interactor;

        mSearchInput.setValue("");
        mIsFetching.setValue(false);
        mShowEmptyMessage.setValue(false);
        mShowErrorMessage.setValue(false);
    }

    public MutableLiveData<List<TrackSummary>> getTrackSummaries() {
        return getTrackSummaries("");
    }

    public MutableLiveData<List<TrackSummary>> getTrackSummaries(String searchInput) {
        if (searchInput != null && !searchInput.isEmpty()) {
            fetchSongs(searchInput);
        }
        return mSongs;
    }

    public MutableLiveData<Boolean> getIsFetching() {
        return mIsFetching;
    }

    public MutableLiveData<Boolean> getShowEmptyMessage() {
        return mShowEmptyMessage;
    }

    public MutableLiveData<Boolean> getShowErrorMessage() {
        return mShowErrorMessage;
    }

    public MutableLiveData<String> getSearchInput() {
        return mSearchInput;
    }

    public TextWatcher getWatcher() {
        return mWatcher;
    }

    public void fetchSongs(String searchInput) {

        mIsFetching.setValue(true);
        mShowEmptyMessage.setValue(false);
        mShowErrorMessage.setValue(false);

        mService.searchTracks(searchInput, this);
    }

    @Override
    public void onReceiveTrackListResponse(boolean success, GetTrackSummaryListResponse response) {

        mIsFetching.setValue(false);

        if (!success) {
            mShowErrorMessage.setValue(true);
            return;
        }

        if (response == null
                || response.results == null
                || response.results.trackmatches == null
                || response.results.trackmatches.mTrackSummaries == null) {
            mShowEmptyMessage.setValue(true);
            return;
        } else if (response.results.trackmatches.mTrackSummaries.size() == 0) {
            mShowEmptyMessage.setValue(true);
        }

        mSongs.setValue(response.results.trackmatches.mTrackSummaries);
    }
}