package com.eluleci.lastfm.ui.trackdetail;

import com.eluleci.lastfm.models.TrackSummary;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.eluleci.lastfm.ui.trackdetail.TrackDetailActivity.PARAM_SONG;

@Module
public abstract class TrackDetailModule {

    @Singleton
    abstract TrackDetailViewModelFactory provideTrackDetailViewModelFactory();

    @Provides
    static TrackSummary provideTrackSummary(TrackDetailActivity activity) {
        return (TrackSummary) activity.getIntent()
                .getExtras().getSerializable(PARAM_SONG);
    }
}
