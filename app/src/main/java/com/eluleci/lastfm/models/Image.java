package com.eluleci.lastfm.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Image implements Serializable {

    public enum Size {
        small, medium, large, extralarge
    }

    @SerializedName("#text")
    private String url;

    private Size size;

    public String getUrl() {
        return url;
    }

    public Size getSize() {
        return size;
    }
}
