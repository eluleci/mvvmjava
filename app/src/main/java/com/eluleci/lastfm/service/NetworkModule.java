package com.eluleci.lastfm.service;

import com.eluleci.lastfm.BuildConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Singleton
    @Provides
    Retrofit provideRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new CommonQueryParamInterceptor(BuildConfig.API_KEY))
                .build();

        return new Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    LastFMService provideLastFMService(Retrofit retrofit) {
        return retrofit.create(LastFMService.class);
    }

    @Singleton
    @Provides
    Interactor provideInteractor(LastFMService service) {
        return new Interactor(service);
    }
}
