package com.eluleci.lastfm.models;

import java.util.List;

public class Album {
    private String artist;
    private String title;
    private List<Image> image;

    public Album() {
    }

    public Album(String artist, String title, List<Image> image) {
        this.artist = artist;
        this.title = title;
        this.image = image;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public Image getImage(Image.Size size) {
        if (image == null) {
            return null;
        }

        for (Image i : image) {
            if (i.getSize() == size) {
                return i;
            }
        }

        return null;
    }
}
