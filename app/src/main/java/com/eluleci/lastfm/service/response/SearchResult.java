package com.eluleci.lastfm.service.response;

import com.google.gson.annotations.SerializedName;

public class SearchResult {
    @SerializedName("trackmatches")
    public Trackmatches trackmatches;

    public SearchResult(Trackmatches trackmatches) {
        this.trackmatches = trackmatches;
    }
}
