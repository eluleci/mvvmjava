package com.eluleci.lastfm.models;

import java.io.Serializable;
import java.util.List;

public class TrackSummary implements Serializable {

    private String artist;
    private String name;
    private List<Image> image;

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }

    public String getArtist() {
        return artist;
    }

    public String getName() {
        return name;
    }

    public Image getImage(Image.Size size) {
        if (image == null) {
            return null;
        }

        for (Image i : image) {
            if (i.getSize() == size) {
                return i;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
