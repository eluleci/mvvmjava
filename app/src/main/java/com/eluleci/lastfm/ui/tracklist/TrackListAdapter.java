package com.eluleci.lastfm.ui.tracklist;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eluleci.lastfm.R;
import com.eluleci.lastfm.databinding.ListRowTrackBinding;
import com.eluleci.lastfm.models.TrackSummary;

import java.util.List;

public class TrackListAdapter extends RecyclerView.Adapter<TrackListAdapter.MyViewHolder> {

    private List<TrackSummary> mTrackSummaries;
    private OnClickListener mClickListener;

    public TrackListAdapter(OnClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void setTrackSummaries(List<TrackSummary> mDataset) {
        this.mTrackSummaries = mDataset;
        notifyDataSetChanged();
    }

    @Override
    public TrackListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListRowTrackBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_row_track,
                parent,
                false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.bind(mTrackSummaries.get(position), mClickListener);
    }

    @Override
    public int getItemCount() {
        return mTrackSummaries != null ? mTrackSummaries.size() : 0;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TrackListItemViewModel viewModel;
        private ListRowTrackBinding binding;

        public MyViewHolder(ListRowTrackBinding binding) {
            super(binding.getRoot());
            this.viewModel = new TrackListItemViewModel();
            this.binding = binding;
        }

        public void bind(final TrackSummary trackSummary, final OnClickListener clickListener) {
            viewModel.bind(trackSummary);
            binding.setViewModel(viewModel);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(trackSummary);
                }
            });
        }
    }

    public interface OnClickListener {
        void onItemClick(TrackSummary trackSummary);
    }
}