package com.eluleci.lastfm.models;

import java.io.Serializable;

public class Track implements Serializable {

    private Artist artist;
    private Album album;
    private String name;
    private String duration;
    private String listeners;

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setListeners(String listeners) {
        this.listeners = listeners;
    }

    public Artist getArtist() {
        return artist;
    }

    public String getName() {
        return name;
    }

    public Album getAlbum() {
        return album;
    }

    public String getListeners() {
        return listeners;
    }

    public String getDuration() {
        return duration;
    }

    public String getDurationFormatted() {
        int seconds = Integer.valueOf(duration) / 1000;
        int minutes = seconds / 60;
        int remainingSeconds = seconds % 60;
        return String.format("%02d:%02d", minutes, remainingSeconds);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
