package com.eluleci.lastfm.ui.tracklist;

import android.arch.lifecycle.ViewModel;

import com.eluleci.lastfm.models.Image;
import com.eluleci.lastfm.models.TrackSummary;

public class TrackListItemViewModel extends ViewModel {

    private String name;
    private String artist;
    private String thumbnailUrl;

    public void bind(TrackSummary trackSummary) {
        name = trackSummary.getName();
        artist = trackSummary.getArtist();

        Image image = trackSummary.getImage(Image.Size.medium);
        if (image != null) {
            thumbnailUrl = image.getUrl();
        }
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }
}