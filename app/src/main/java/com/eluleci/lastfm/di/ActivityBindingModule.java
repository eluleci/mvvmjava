package com.eluleci.lastfm.di;

import com.eluleci.lastfm.ui.trackdetail.TrackDetailActivity;
import com.eluleci.lastfm.ui.trackdetail.TrackDetailModule;
import com.eluleci.lastfm.ui.tracklist.TrackListActivity;
import com.eluleci.lastfm.ui.tracklist.TrackListModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = TrackListModule.class)
    abstract TrackListActivity trackListActivity();

    @ContributesAndroidInjector(modules = TrackDetailModule.class)
    abstract TrackDetailActivity trackDetailActivity();
}