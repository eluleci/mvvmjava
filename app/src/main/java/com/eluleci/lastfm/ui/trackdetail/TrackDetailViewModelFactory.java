package com.eluleci.lastfm.ui.trackdetail;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.eluleci.lastfm.models.TrackSummary;

import javax.inject.Inject;

public class TrackDetailViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    @Inject
    public TrackDetailViewModel mTrackDetailViewModel;

    @Inject
    public TrackDetailViewModelFactory() {
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) mTrackDetailViewModel;
    }
}