package com.eluleci.lastfm.service;

import com.eluleci.lastfm.service.response.GetTrackInfoResponse;
import com.eluleci.lastfm.service.response.GetTrackSummaryListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LastFMService {

    @GET("/2.0/?method=track.search")
    Call<GetTrackSummaryListResponse> searchTracks(@Query("track") String searchInput);

    @GET("/2.0/?method=track.getInfo")
    Call<GetTrackInfoResponse> getTrackDetail(@Query("artist") String artist, @Query("track") String track);
}