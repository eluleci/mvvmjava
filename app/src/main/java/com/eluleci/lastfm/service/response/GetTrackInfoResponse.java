package com.eluleci.lastfm.service.response;

import com.eluleci.lastfm.models.Track;
import com.google.gson.annotations.SerializedName;

public class GetTrackInfoResponse {
    @SerializedName("track")
    public Track track;

    public GetTrackInfoResponse() {
    }

    public GetTrackInfoResponse(Track track) {
        this.track = track;
    }
}

