package com.eluleci.lastfm.ui.tracklist;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TrackListViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    @Inject
    TrackListViewModel mViewModel;

    @Inject
    public TrackListViewModelFactory() {
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) mViewModel;
    }
}