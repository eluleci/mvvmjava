package com.eluleci.lastfm.service.response;

import com.eluleci.lastfm.models.TrackSummary;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Trackmatches {
    @SerializedName("track")
    public List<TrackSummary> mTrackSummaries;

    public Trackmatches(List<TrackSummary> trackSummaries) {
        mTrackSummaries = trackSummaries;
    }
}
